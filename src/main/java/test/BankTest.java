package test;

import junit.framework.TestCase;
import model.Account;
import model.Bank;
import model.Person;
import model.SpendingAccount;

public class BankTest extends TestCase {
		
	Bank bank = new Bank();
	Person p = new Person();
	Account a = new SpendingAccount();
	
	
	public void testNumberOfSpendingAccounts() {
		p.setCnp("1242213213421");
		a.setMoney(1);
		bank.addNewClient(p, a);
		assertEquals(0,bank.getNumberOfSpendingAccounts(p));
	}
	
	public void testCheckPerson() {
		
		Person p1 = new Person();
		p.setCnp("1242213213421");
		a.setMoney(1);
		bank.addNewClient(p, a);
		p1.setCnp("1242213213421");
		assertEquals(true,bank.checkPerson(p1));
	}
	
	
}
