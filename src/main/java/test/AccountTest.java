package test;

import junit.framework.TestCase;
import model.Account;
import model.SavingAccount;
import model.SpendingAccount;

public class AccountTest extends TestCase {
	
	Account a;
	
	public void testWithdraw() {

		Account a1 = new SpendingAccount();
		a1.setMoney(300);
		double money = 200;
		((SpendingAccount) a1).withdrawMoney(money);
		assertEquals(300.0, a1.getMoney());
	}
	
	public void testAccType() {
		
		a = new SavingAccount();
		
		assertEquals("Saving Account",a.getAccountType(a));
	}
	
	public void testCheckNameFormat() {
		
		a = new SpendingAccount();
		assertEquals(false,a.checkNameFormat("1Sigur"));
	}

}
