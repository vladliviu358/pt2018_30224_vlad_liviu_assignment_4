package test;

import junit.framework.TestCase;
import model.Person;

public class PersonTest extends TestCase {
	
	Person p;
	
	public void testCheckAddressFormat() {
		p = new Person();
		String s = "Grigorescu";
		p.setAddress(s);
		assertEquals(true, p.checkAddressFormat(s));
	}
	
	public void testCheckPhoneFormat() {
		
		p = new Person();
		String phone = "0123";
		p.setPhone(phone);
		assertEquals(true,p.checkPhoneFormat(phone));
	}
	
	public void testEquals() {
		
		Person p1 = new Person();
		p1.setAddress("DA");
		p = new Person();
		p.setAddress("DA");
		assertEquals(true,p.equals(p1));
	}
	
}
