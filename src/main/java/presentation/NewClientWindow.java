package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.awt.event.ActionEvent;

public class NewClientWindow {

	public JFrame frame;
	private JTextField cnpText;
	private JTextField nameText;
	private JTextField pinText;
	private JTextField addressText;
	private JTextField mailText;
	private JTextField phoneText;
	private Bank bank;
	private Person person;
	private Set<Account> accounts;


	/**
	 * Create the application.
	 */
	public NewClientWindow(Bank bank) {
		this.bank = bank;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 204, 153));
		frame.getContentPane().setForeground(Color.ORANGE);
		frame.setBounds(100, 100, 450, 417);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Client Nou");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		lblNewLabel.setBounds(143, 13, 145, 52);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel label0 = new JLabel("CNP");
		label0.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		label0.setBounds(97, 78, 94, 30);
		frame.getContentPane().add(label0);
		
		JLabel label3 = new JLabel("Nume");
		label3.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		label3.setBounds(97, 121, 94, 30);
		frame.getContentPane().add(label3);
		
		JLabel label2 = new JLabel("Pin");
		label2.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		label2.setBounds(97, 164, 94, 30);
		frame.getContentPane().add(label2);
		
		JLabel label92 = new JLabel("Adresa");
		label92.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		label92.setBounds(97, 207, 94, 30);
		frame.getContentPane().add(label92);
		
		cnpText = new JTextField();
		cnpText.setBounds(203, 83, 116, 22);
		frame.getContentPane().add(cnpText);
		cnpText.setColumns(10);
		
		nameText = new JTextField();
		nameText.setColumns(10);
		nameText.setBounds(203, 126, 116, 22);
		frame.getContentPane().add(nameText);
		
		pinText = new JTextField();
		pinText.setColumns(10);
		pinText.setBounds(203, 169, 116, 22);
		frame.getContentPane().add(pinText);
		
		addressText = new JTextField();
		addressText.setColumns(10);
		addressText.setBounds(203, 212, 116, 22);
		frame.getContentPane().add(addressText);
		
		mailText = new JTextField();
		mailText.setColumns(10);
		mailText.setBounds(203, 255, 116, 22);
		frame.getContentPane().add(mailText);
		
		phoneText = new JTextField();
		phoneText.setColumns(10);
		phoneText.setBounds(203, 290, 116, 22);
		frame.getContentPane().add(phoneText);
		
		JLabel lblEmail = new JLabel("E-Mail");
		lblEmail.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblEmail.setBounds(97, 250, 94, 30);
		frame.getContentPane().add(lblEmail);
		
		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblTelefon.setBounds(97, 285, 94, 30);
		frame.getContentPane().add(lblTelefon);
		
		
		String[] accountType = {"Saving Account","Spending Account"};
		
		JButton addClient = new JButton("Adauga");
		addClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = nameText.getText();
				String cnp = cnpText.getText();
				String address = addressText.getText();
				String phone = phoneText.getText();
				String mail = mailText.getText();
				String pin = pinText.getText();
				
				person = new Person();
				person.setName(name);
				person.setCnp(cnp);
				person.setMail(mail);
				person.setPhone(phone);
				person.setPin(pin);
				person.setAddress(address);
				Account a = null;
				
//				int aType = comboBox.getSelectedIndex();
//
//				switch(aType) {
//				case 0: a = new SavingAccount(); break;
//				case 1: a = new SpendingAccount(); break;
//				}
				a = new SavingAccount();
				a.setName("Test");
				a.setMoney(1);
				a.setExpDate("00.00.0000");
				bank.addNewClient(person,a);
				if(!(person.equals(null))) {
					JOptionPane.showMessageDialog(null, "Clientul a fost adaugat", "Info Panel", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});
		addClient.setBounds(222, 334, 97, 25);
		frame.getContentPane().add(addClient);
		
		
		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AdminWindow aw = new AdminWindow(bank);
				aw.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setBounds(97, 334, 97, 25);
		frame.getContentPane().add(back);
	}
}
