package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class ClientWindow extends JPanel{

	public JFrame frame;
	private JTextField textField;
	private JTable table;


	/**
	 * Create the application.
	 */
	public ClientWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(51, 153, 153));
		frame.setBounds(100, 100, 800, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton button = new JButton("Inapoi");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Home home = new Home();
				home.frame.setVisible(true);
				frame.dispose();
			}
		});
		button.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		button.setBounds(23, 32, 109, 34);
		frame.getContentPane().add(button);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(186, 35, 372, 32);
		frame.getContentPane().add(textField);
		
		JButton button_1 = new JButton("Cauta");
		button_1.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		button_1.setBounds(590, 32, 97, 32);
		frame.getContentPane().add(button_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(144, 86, 626, 322);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		table.setRowHeight(40);
		table.setForeground(new Color(255, 255, 224));
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
		table.setBackground(Color.BLACK);
		scrollPane.setViewportView(table);
		
	}
}
