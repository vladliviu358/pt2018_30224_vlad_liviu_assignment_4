package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import model.Account;
import model.Bank;
import model.Person;

public class Home {

	public JFrame frame;
	private Bank bank ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home window = new Home();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Home() {
		bank = new Bank();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(75, 0, 130));
		frame.setBounds(100, 100, 800, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bank Application");
		lblNewLabel.setForeground(new Color(250, 250, 210));
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 34));
		lblNewLabel.setBounds(251, 91, 258, 141);
		frame.getContentPane().add(lblNewLabel);
		
		JButton adminWindow = new JButton("Administrator");
		adminWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminWindow adminW = new AdminWindow(bank);
				adminW.frame.setVisible(true);
				bank.readData();
				frame.dispose();
				
			}
		});
		adminWindow.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		adminWindow.setBounds(133, 310, 181, 60);
		frame.getContentPane().add(adminWindow);
		
		JButton customerWindow = new JButton("Clienti");
		customerWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				LoginWindo lw = new LoginWindo();
				lw.frame.setVisible(true);
				frame.dispose();
			}
		});
		customerWindow.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		customerWindow.setBounds(422, 310, 181, 60);
		frame.getContentPane().add(customerWindow);
		frame.setResizable(false);
	}
	
}
