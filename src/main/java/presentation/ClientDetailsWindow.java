package presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import org.omg.Messaging.SyncScopeHelper;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ClientDetailsWindow {

	public JFrame frame;
	private Bank bank;
	private JTable table;
	private String cnp;
	private JLabel lblNewLabel;
	private JButton deleteAccount;
	private JButton changeAccount;
	private JLabel lblNume;
	private JTextField nameText;
	private JTextField moneyText;
	private JLabel sumText;
	private JButton addMoney;
	private JButton withdrawMoney;

	/**
	 * Create the application.
	 */
	public ClientDetailsWindow(Bank bank, String cnp) {
		this.bank = bank;
		this.cnp = cnp;
		initialize();
		initialInfo();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 250, 240));
		frame.setBounds(100, 100, 669, 515);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 53, 626, 322);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		table.setRowHeight(40);
		table.setForeground(new Color(255, 255, 224));
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
		table.setBackground(Color.BLACK);
		scrollPane.setViewportView(table);

		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				AdminWindow aw = new AdminWindow(bank);
				aw.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		back.setBounds(12, 13, 109, 34);
		frame.getContentPane().add(back);

		lblNewLabel = new JLabel("Conturi Client");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblNewLabel.setBounds(257, 13, 120, 27);
		frame.getContentPane().add(lblNewLabel);

		deleteAccount = new JButton("Sterge");
		deleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int row = table.getSelectedRow();

				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un cont", "No account selected",
							JOptionPane.INFORMATION_MESSAGE);
				}

				if (row >= 0) {
					String cnp = table.getModel().getValueAt(row, 0).toString();
					String nume = table.getModel().getValueAt(row, 2).toString();
					for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {

						if (entry.getKey().getCnp().equals(cnp)) {
							for (Account a : entry.getValue()) {
								if (a.getName().equals(nume)) {
									bank.deleteAccount(entry.getKey(), a);
									System.out.println("Successfully Deleted");
								}
							}
						}
					}
				}
				initialInfo();
			}
		});
		deleteAccount.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		deleteAccount.setBounds(216, 388, 109, 34);
		frame.getContentPane().add(deleteAccount);

		changeAccount = new JButton("Modifica");
		changeAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String name = nameText.getText();

				int row = table.getSelectedRow();

				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un cont", "No account selected",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if (row >= 0) {
					String cnp = table.getValueAt(row, 0).toString();
					String dataExp = table.getValueAt(row, 4).toString();
					for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
						if (entry.getKey().getCnp().equals(cnp)) {
							for (Account a : entry.getValue()) {
								if (a.getExpDate().equals(dataExp)) {
									a.setName(name);
									
								}
							}

						}
					}
				}
				initialInfo();

			}
		});
		changeAccount.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		changeAccount.setBounds(216, 435, 109, 34);
		frame.getContentPane().add(changeAccount);

		lblNume = new JLabel("Nume");
		lblNume.setBounds(12, 388, 56, 16);
		frame.getContentPane().add(lblNume);

		nameText = new JTextField();
		nameText.setBounds(69, 388, 116, 22);
		frame.getContentPane().add(nameText);
		nameText.setColumns(10);

		moneyText = new JTextField();
		moneyText.setBounds(493, 388, 116, 22);
		frame.getContentPane().add(moneyText);
		moneyText.setColumns(10);

		sumText = new JLabel("Suma");
		sumText.setBounds(425, 388, 56, 16);
		frame.getContentPane().add(sumText);

		addMoney = new JButton("Depune");
		addMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int row = table.getSelectedRow();
				double money = (Double) table.getValueAt(row, 3);
				double moneyToAdd = Double.parseDouble(moneyText.getText());
				double totalToAdd = money + moneyToAdd;
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un cont", "No account selected",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if (row >= 0) {
					String cnp = table.getValueAt(row, 0).toString();
					String nume = table.getValueAt(row, 2).toString();
					for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
						if (entry.getKey().getCnp().equals(cnp)) {
							for (Account a : entry.getValue()) {
								if (a.getName().equals(nume)) {
									if (a.getAccountType(a).equals("Spending Account")) {
										JOptionPane.showMessageDialog(null, "Nu puteti depune bani in acest cont",
												"Deposit Warning", JOptionPane.INFORMATION_MESSAGE);
									} else if (a.getAccountType(a).equals("Saving Account")) {
										a.setMoney(totalToAdd);
									}

								}
							}

						}
					}
				}
				initialInfo();
			}
		});
		addMoney.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		addMoney.setBounds(392, 428, 109, 34);
		frame.getContentPane().add(addMoney);

		withdrawMoney = new JButton("Retrage");
		withdrawMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int row = table.getSelectedRow();
				double money = (Double) table.getValueAt(row, 3);
				double moneyToWithdraw = Double.parseDouble(moneyText.getText());
				double totalToWithdraw = money - moneyToWithdraw;
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un cont", "No account selected",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if (row >= 0) {
					String cnp = table.getValueAt(row, 0).toString();
					String nume = table.getValueAt(row, 2).toString();
					for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
						if (entry.getKey().getCnp().equals(cnp)) {
							for (Account a : entry.getValue()) {
								if (a.getName().equals(nume)) {
									if (a.getAccountType(a).equals("Saving Account")) {
										JOptionPane.showMessageDialog(null, "Nu puteti retrage bani in acest cont",
												"Deposit Warning", JOptionPane.INFORMATION_MESSAGE);
									} else if (a.getAccountType(a).equals("Spending Account")) {
										if(moneyToWithdraw > money) {
											JOptionPane.showMessageDialog(null, "Sold insuficient",
													"Deposit Warning", JOptionPane.INFORMATION_MESSAGE);
										}
										else
										a.setMoney(totalToWithdraw);
									}

								}
							}

						}
					}
				}
				initialInfo();

			}
		});
		withdrawMoney.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		withdrawMoney.setBounds(518, 428, 109, 34);
		frame.getContentPane().add(withdrawMoney);
		frame.setResizable(false);

	}

	public void initialInfo() {

		DefaultTableModel tm = new DefaultTableModel();
		tm.addColumn("CNP");
		tm.addColumn("PIN");
		tm.addColumn("Cont");
		tm.addColumn("Balanta");
		tm.addColumn("Data Exp");
		tm.addColumn("Tip Cont");
		for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
			if (entry.getKey().getCnp().equals(cnp)) {
				System.out.println("CNP check " + entry.getKey().getCnp());
				System.out.println(entry.getValue());
				for (Account a : entry.getValue()) {
					tm.addRow(new Object[] { entry.getKey().getCnp(), entry.getKey().getPin(), a.getName(),
							a.getMoney(), a.getExpDate(), a.getAccountType(a)

					});
				}
			}
		}

		table.setModel(tm);
	}
}
