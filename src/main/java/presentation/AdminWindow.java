package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import model.Account;
import model.Bank;
import model.Person;

import java.awt.Color;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;

public class AdminWindow extends JPanel {

	public JFrame frame;
	private JTable table;
	private Bank bank;
	private Person person;
	private JTextField nameText;
	private JTextField addressText;
	private JTextField phoneText;
	private JTextField mailText;
	private JTextField pinText;

	/**
	 * Create the application.
	 */
	public AdminWindow(Bank bank) {
		this.bank = bank;
		initialize();
		infoTable();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(51, 102, 153));
		frame.setBounds(100, 100, 800, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Home home = new Home();
				home.frame.setVisible(true);
				frame.dispose();

			}
		});
		back.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		back.setBounds(12, 24, 109, 34);
		frame.getContentPane().add(back);

		JButton showClients = new JButton("Clienti");
		showClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				infoTable();
			}
		});
		showClients.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		showClients.setBounds(12, 176, 109, 34);
		frame.getContentPane().add(showClients);

		JButton newAccount = new JButton("Cont Nou");
		newAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				NewAccountWindow nAccount = new NewAccountWindow(bank);
				nAccount.frame.setVisible(true);
				frame.dispose();

			}
		});
		newAccount.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		newAccount.setBounds(12, 270, 109, 34);
		frame.getContentPane().add(newAccount);

		JButton newClient = new JButton("Client Nou");
		newClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewClientWindow ncw = new NewClientWindow(bank);
				ncw.frame.setVisible(true);
				frame.dispose();

			}
		});
		newClient.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		newClient.setBounds(12, 223, 109, 34);
		frame.getContentPane().add(newClient);

		JButton deletePerson = new JButton("Sterge");
		deletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				person = new Person();
				int row = table.getSelectedRow();

				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un client", "No client selected",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if(row >= 0) {
					String cnp = table.getValueAt(row, 1).toString();
					for(Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
						if(entry.getKey().getCnp().equals(cnp)) {
							bank.deletePerson(entry.getKey());
							System.out.println(entry.getKey().toString());
							
						}
					}
				}
					infoTable();
			}
		});
		deletePerson.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		deletePerson.setBounds(201, 424, 168, 34);
		frame.getContentPane().add(deletePerson);
		
		JButton btnModifica = new JButton("Modifica");
		btnModifica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = nameText.getText();
				String address = addressText.getText();
				String phone = phoneText.getText();
				String pin = pinText.getText();
				String mail = mailText.getText();
				
				int row = table.getSelectedRow();

				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un client", "No client selected",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if(row >= 0) {
					String cnp = table.getValueAt(row, 1).toString();
					for(Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
						if(entry.getKey().getCnp().equals(cnp)) {
							entry.getKey().setName(name);
							entry.getKey().setMail(mail);
							entry.getKey().setAddress(address);
							entry.getKey().setPhone(phone);
							entry.getKey().setPin(pin);
							System.out.println(entry.getKey().toString());
							
						}
					}
				}
					infoTable();
			}
		});
		btnModifica.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		btnModifica.setBounds(561, 424, 168, 34);
		frame.getContentPane().add(btnModifica);

		JButton personDetails = new JButton("Detalii Persoana");
		personDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int row = table.getSelectedRow();
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un client", "Invalid choice",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if (row >= 0) {
					String cnp = table.getModel().getValueAt(row, 1).toString();
					System.out.println(cnp);
					ClientDetailsWindow cdw = new ClientDetailsWindow(bank, cnp);
					cdw.frame.setVisible(true);
					frame.dispose();
				}
			}
		});
		personDetails.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		personDetails.setBounds(381, 424, 168, 34);
		frame.getContentPane().add(personDetails);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(144, 86, 626, 322);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		table.setRowHeight(40);
		table.setForeground(new Color(255, 255, 224));
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
		table.setBackground(Color.BLACK);
		scrollPane.setViewportView(table);

		JButton loadData = new JButton("Load");
		loadData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				bank.writeData();
			}
		});
		loadData.setFont(UIManager.getFont("CheckBoxMenuItem.font"));
		loadData.setBounds(12, 317, 109, 34);
		frame.getContentPane().add(loadData);
		
		JLabel lblNewLabel = new JLabel("Nume");
		lblNewLabel.setBounds(201, 13, 56, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblAdresa = new JLabel("Adresa");
		lblAdresa.setBounds(201, 42, 56, 16);
		frame.getContentPane().add(lblAdresa);
		
		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setBounds(416, 13, 56, 16);
		frame.getContentPane().add(lblTelefon);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(416, 42, 56, 16);
		frame.getContentPane().add(lblMail);
		
		nameText = new JTextField();
		nameText.setBounds(269, 10, 116, 22);
		frame.getContentPane().add(nameText);
		nameText.setColumns(10);
		
		addressText = new JTextField();
		addressText.setColumns(10);
		addressText.setBounds(269, 36, 116, 22);
		frame.getContentPane().add(addressText);
		
		phoneText = new JTextField();
		phoneText.setColumns(10);
		phoneText.setBounds(484, 10, 116, 22);
		frame.getContentPane().add(phoneText);
		
		mailText = new JTextField();
		mailText.setColumns(10);
		mailText.setBounds(484, 39, 116, 22);
		frame.getContentPane().add(mailText);
		
		pinText = new JTextField();
		pinText.setColumns(10);
		pinText.setBounds(654, 10, 116, 22);
		frame.getContentPane().add(pinText);
		
		JLabel lblPin = new JLabel("PIN");
		lblPin.setBounds(612, 13, 33, 16);
		frame.getContentPane().add(lblPin);
		
	}

	public void infoTable() {

		DefaultTableModel tm = new DefaultTableModel();
		tm.addColumn("Nume");
		tm.addColumn("CNP");
		tm.addColumn("Adresa");
		tm.addColumn("Telefon");
		tm.addColumn("PIN");
		tm.addColumn("Mail");

		for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {

			tm.addRow(new Object[] { entry.getKey().getName(), entry.getKey().getCnp(), entry.getKey().getAddress(),
					entry.getKey().getPhone(), entry.getKey().getPin(), entry.getKey().getMail() });
		}

		table.setModel(tm);

	}
}
