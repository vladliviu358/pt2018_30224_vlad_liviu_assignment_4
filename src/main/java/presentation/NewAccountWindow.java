package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class NewAccountWindow {

	public JFrame frame;
	private JTextField cnpClientText;
	private JTextField nameText;
	private JTextField balanceText;
	private Bank bank;
	private JTextField dataExpText;
	private Account account;


	/**
	 * Create the application.
	 */
	public NewAccountWindow(Bank bank) {
		this.bank = bank;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 204, 255));
		frame.setBounds(100, 100, 380, 362);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cont Nou");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		lblNewLabel.setBounds(125, 13, 107, 51);
		frame.getContentPane().add(lblNewLabel);
		
		cnpClientText = new JTextField();
		cnpClientText.setBounds(165, 77, 116, 22);
		frame.getContentPane().add(cnpClientText);
		cnpClientText.setColumns(10);
		
		nameText = new JTextField();
		nameText.setColumns(10);
		nameText.setBounds(165, 112, 116, 22);
		frame.getContentPane().add(nameText);
		
		balanceText = new JTextField();
		balanceText.setColumns(10);
		balanceText.setBounds(165, 147, 116, 22);
		frame.getContentPane().add(balanceText);
		
		JLabel labell = new JLabel("CNP Client");
		labell.setBounds(54, 80, 99, 16);
		frame.getContentPane().add(labell);
		
		JLabel lblBalanta = new JLabel("Nume");
		lblBalanta.setBounds(54, 115, 56, 16);
		frame.getContentPane().add(lblBalanta);
		
		JLabel lblDataExp = new JLabel("Balanta");
		lblDataExp.setBounds(54, 150, 56, 16);
		frame.getContentPane().add(lblDataExp);
		
		String[] accountType = {"Saving Account","Spending Account"};
		final JComboBox comboBox = new JComboBox(accountType);
		comboBox.setMaximumRowCount(2);
		comboBox.setBounds(165, 215, 116, 22);
		frame.getContentPane().add(comboBox);
		
		JButton addAccount = new JButton("Adauga");
		addAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nume = nameText.getText();
				String dataExp = dataExpText.getText();
				double balance = Double.parseDouble(balanceText.getText());
				String cnp = cnpClientText.getText();
				
				int aType = comboBox.getSelectedIndex();

				switch(aType) {
				case 0: account = new SavingAccount(); break;
				case 1: account = new SpendingAccount(); break;
				}
				System.out.println(account.getAccountType(account));
				
				for (Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
					
					if(entry.getKey().getCnp().equals(cnp)) {
						
						account.setExpDate(dataExp);
						account.setName(nume);
						account.setMoney(balance);
						bank.addNewClientAccount(entry.getKey(), account);
						JOptionPane.showMessageDialog(null, "Contul a fost adaugat", "Cont Nou", JOptionPane.INFORMATION_MESSAGE);
						System.out.println(account.toString());
					}
				}
				if(account.getName().equals(null)) {
					JOptionPane.showMessageDialog(null, "Clientul nu a putut fi gasit", "Cont Nou", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		addAccount.setBounds(184, 254, 97, 25);
		frame.getContentPane().add(addAccount);
		
		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminWindow aWindow = new AdminWindow(bank);
				aWindow.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setBounds(54, 254, 97, 25);
		frame.getContentPane().add(back);
			
		JLabel label = new JLabel("Tipul Contului");
		label.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label.setBounds(54, 211, 94, 30);
		frame.getContentPane().add(label);
		
		dataExpText = new JTextField();
		dataExpText.setColumns(10);
		dataExpText.setBounds(165, 180, 116, 22);
		frame.getContentPane().add(dataExpText);
		
		JLabel label_1 = new JLabel("Data Exp");
		label_1.setBounds(54, 182, 56, 16);
		frame.getContentPane().add(label_1);
	}
}
