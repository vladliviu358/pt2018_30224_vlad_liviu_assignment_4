package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginWindo {

	public JFrame frame;
	private JTextField cnpField;
	private JTextField pinField;


	/**
	 * Create the application.
	 */
	public LoginWindo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(128, 128, 128));
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Panou de autentificare");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 30));
		lblNewLabel.setBounds(221, 68, 273, 97);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("CNP");
		lblNewLabel_1.setForeground(new Color(0, 0, 128));
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(166, 205, 103, 36);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblPin = new JLabel("PIN");
		lblPin.setForeground(new Color(0, 0, 128));
		lblPin.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblPin.setBounds(166, 254, 103, 36);
		frame.getContentPane().add(lblPin);
		
		cnpField = new JTextField();
		cnpField.setBounds(284, 214, 229, 22);
		frame.getContentPane().add(cnpField);
		cnpField.setColumns(10);
		
		pinField = new JTextField();
		pinField.setColumns(10);
		pinField.setBounds(284, 265, 229, 22);
		frame.getContentPane().add(pinField);
		
		JButton login = new JButton("Login");
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ClientWindow cw = new ClientWindow();
				cw.frame.setVisible(true);
				frame.dispose();
			}
		});
		login.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		login.setBounds(378, 353, 116, 36);
		frame.getContentPane().add(login);
		
		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Home home = new Home();
				home.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		back.setBounds(196, 353, 116, 36);
		frame.getContentPane().add(back);
		frame.setBounds(100, 100, 800, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
