package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

public class Bank implements BankProc, Serializable {

	private static final long serialVersionUID = 1L;
	private HashMap<Person, Set<Account>> bankHashMap;
	private Set<Person> persons;

	public Bank() {
		bankHashMap = new HashMap<Person, Set<Account>>();
		persons = new HashSet<Person>();
	}

	public HashMap<Person, Set<Account>> getHashMap() {

		return this.bankHashMap;
	}

	public Set<Person> getPersons() {
		
		for (Map.Entry<Person, Set<Account>> entry : bankHashMap.entrySet()) {
			
			persons.add(entry.getKey());
		}
		return this.persons;

	}

	public Set<Account> getAccountForPerson(Person person) {

		Set<Account> accounts = new HashSet<Account>();
		accounts = bankHashMap.get(person);
		return accounts;
	}

	public void addNewClient(Person p, Account a) {

		assert checkBank() : "Contul unui client are balanta zero";
		assert findCNP(p.getCnp()) : "Persoana cu CNP-ul: " + p.getCnp() + " este deja inregistrata";
		int countPersonBeforeAdd = persons.size();
		persons.add(p);
		Set<Account> accounts = new HashSet<Account>();
		accounts.add(a);
		bankHashMap.put(p, accounts);
		int countPersonAfterAdd = persons.size();
		int countAccountsFromHash = bankHashMap.get(p).size();
		assert (countPersonAfterAdd == countPersonBeforeAdd + 1) : "Persoana nu a putut fi adaugata";
		assert (countAccountsFromHash > 0) : "Contul nu a putut fi adaugat";
		assert checkBank() : "Contul unui client are balanta zero";
	}

	public void addNewClientAccount(Person p, Account a) {

		assert checkBank() : "Contul unui client are balanta zero";
		assert (a != null) : "Contul este invalid";
		assert (p != null) : "Clientul este invalid";
		int countAccountBeforeAdd = bankHashMap.get(p).size();
		bankHashMap.get(p).add(a);
		int countAccountAfterAdd = bankHashMap.get(p).size();
		assert (countAccountAfterAdd == countAccountBeforeAdd + 1) : "Contul nu a putut fi adaugat";
		assert checkBank() : "Contul unui client are balanta zero";
	}

	public void deletePerson(Person p) {

		assert checkBank() : "Contul unui client are balanta zero";
		assert (p != null) : "Clientul selectat este invalid";
		int size = bankHashMap.size();
		bankHashMap.remove(p);
		assert (bankHashMap.size() == size - 1) : "Erroare la stergerea clientului";
		assert checkBank() : "Contul unui client are balanta zero";
	}

	public void deleteAccount(Person p, Account a) {
		assert checkBank() : "Contul unui client are balanta zero";
		assert (p != null) : "Contul este invalid";
		int size = bankHashMap.get(p).size();
		bankHashMap.get(p).remove(a);
		assert (bankHashMap.get(p).size() == size - 1) : "Contul nu a putut fi sters";
		assert checkBank() : "Contul unui client are balanta zero";
	}

	public boolean findCNP(String cnp) {

		for (Person p : persons) {

			if (p.getCnp().equals(cnp)) {
				JOptionPane.showMessageDialog(null, "CNP-ul ales este deja utilizat", "CNP Match",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return true;
	}

	public boolean checkPerson(Person p) {

		boolean check = false;

		for (Person p1 : persons) {

			if (p1.equals(p)) {

				check = true;
			}
		}

		return check;
	}

	public Person findPersonByCNP(String cnp) {

		for (Person p : persons) {

			if (p.getCnp().equals(cnp)) {

				return p;
			}
		}
		return null;
	}

	public Account findAccountByName(Person p, String name) {

		assert checkBank() : "Contul unui client are balanta zero";
		Set<Account> setAccount;

		for (Map.Entry<Person, Set<Account>> entry : bankHashMap.entrySet()) {

			setAccount = new HashSet<Account>();
			setAccount = entry.getValue();
			for (Account a1 : setAccount) {
				if (a1.getName() == name) {
					return a1;
				}
			}
		}
		assert checkBank() : "Contul unui client are balanta zero";
		return null;
	}

	public int getNumberOfSavingsAccounts(Person p) {

		assert checkBank() : "Contul unui client are balanta zero";
		Set<Account> accounts = new HashSet<Account>();
		if (getAccountForPerson(p) != null) {
			accounts = getAccountForPerson(p);
		}

		int number = 0;
		for (Account a : accounts) {
			if (a.toString().contains("Saving Account")) {
				number++;
			}
		}
		assert checkBank() : "Contul unui client are balanta zero";
		return number;
	}

	public int getNumberOfSpendingAccounts(Person p) {
		assert checkBank() : "Contul unui client are balanta zero";
		Set<Account> accounts = new HashSet<Account>();
		if (getAccountForPerson(p) != null) {
			accounts = getAccountForPerson(p);
		}

		int number = 0;
		for (Account a : accounts) {
			if (a.toString().contains("Spending Account")) {
				number++;
			}
		}
		assert checkBank() : "Contul unui client are balanta zero";
		return number;
	}

	public void depositMoney(Person p, Account a, double money) {

		for (Map.Entry<Person, Set<Account>> entry : bankHashMap.entrySet()) {

			if (entry.getKey() == p) {
				if (entry.getValue().contains(a) == true) {
					((SavingAccount) a).depositMoney(money);
				}
			}
		}

	}

	public void widthdrawMoney(Person p, Account a, double money) {

		for (Map.Entry<Person, Set<Account>> entry : bankHashMap.entrySet()) {

			if (entry.getKey() == p) {
				if (entry.getValue().contains(a) == true) {
					((SpendingAccount) a).withdrawMoney(money);
				}
			}
		}
	}

	public boolean checkBank() {
		
		boolean check = true;
		for (Map.Entry<Person, Set<Account>> entry : bankHashMap.entrySet()) {
				if(entry.getKey().equals(null)){
					check = false;
				}
				 for(Account a : entry.getValue()) {
					 if(a.getMoney() == 0){
						 check = false;
					 }
					
				}
		}
		return check;
	}

	public void writeData() {

		try {
			FileOutputStream fileOut = new FileOutputStream("E:\\Java\\Proiecte Java\\TP_Assignment4/Bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(bankHashMap);
			out.close();
			System.out.println("Data saved successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public void readData() {
		//HashMap<Person, Set<Account>> newMap = new HashMap<Person,Set<Account>>();
		try {
			FileInputStream fileIn = new FileInputStream("E:\\Java\\Proiecte Java\\TP_Assignment4/Bank.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
		bankHashMap = (HashMap<Person, Set<Account>>) in.readObject();
			fileIn.close();
			System.out.println("Data read successfully");
		} catch (IOException e1) {

			e1.printStackTrace();
		} catch (ClassNotFoundException e2) {

			e2.printStackTrace();
		}
	}
	
}
