package model;

import java.io.Serializable;

public class Person implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private String cnp;
	private String pin;
	private String address;
	private String mail;
	private String phone;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {

		assert checkCNPFormat(cnp) : "CNP-ul introdus nu este valid";
		this.cnp = cnp;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {

		assert checkPinFormat(pin) : "Codul pin trebuie sa contina 4 caractere";
		this.pin = pin;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {

		assert checkAddressFormat(address) : "Adresa Invalida";
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {

		assert checkMailFormat(mail) : "Adresa de e-mail nu este valida";
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		
		assert checkPhoneFormat(phone):"Numar invalid";
		this.phone = phone;
	}

	public boolean equals(Object object) {

		if (object == null)
			return false;
		if (!(object instanceof Person))
			return false;

		Person person = (Person) object;
		if (this.name != person.name)
			return false;
		if (this.cnp != person.cnp)
			return false;
		if (this.mail != person.mail)
			return false;
		if (this.address != person.address)
			return false;
		if (this.pin != person.pin)
			return false;

		return true;
	}

	public int hashCode() {

		int cnp = Integer.parseInt(this.cnp.substring(0, 10));
		int hash = 1;
		hash = hash * 37 + cnp - 17;
		return hash;
	}

	public boolean checkCNPFormat(String cnp) {

		boolean check = true;
		if (cnp.length() != 13) {
			check = false;
		}
		return check;
	}

	public boolean checkMailFormat(String mail) {

		if (mail.contains("@") == false) {
			return false;
		}
		return true;
	}

	public boolean checkAddressFormat(String address) {

		if (Character.isDigit(address.charAt(0)) == true) {
			return false;
		}

		return true;
	}

	public boolean checkPhoneFormat(String phone) {

		if (phone.charAt(0) != '0') {
			return false;
		}
		return true;
	}

	public boolean checkPinFormat(String pin) {

		if (pin.length() < 4) {
			return false;
		}
		return true;
	}

	public String toString() {

		return "Name: " + this.getName() + " CNP: " + this.getCnp() + " Address: " + this.getAddress() + " PIN: "
				+ this.getPin() + " Mail: " + this.getMail();
	}

}
