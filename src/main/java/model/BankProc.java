package model;

public interface BankProc {
	
	public void addNewClient(Person p, Account a);
	public void addNewClientAccount(Person p, Account a);
	public void deletePerson(Person p);
	public void deleteAccount(Person p, Account a);
}
