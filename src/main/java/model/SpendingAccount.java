package model;
import java.io.Serializable;
public class SpendingAccount extends Account implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public SpendingAccount() {
		super();
	}
	
	public void withdrawMoney(double money) {
		
		assert(money <=10000):"Maximul sumei este de 10000 RON";
		assert(money > 1):"Minimul sumei este de 1 ROM";
		double oldBalance = super.getMoney();
		assert(money < oldBalance):"Fonduri insuficiente";
		double newBalance = oldBalance - money;
		super.getMoney();
		assert(newBalance >= 0):"Eroare la retragere!";
	}

}
