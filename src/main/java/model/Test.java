package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		Person p1 = new Person();
		Person p2 = new Person();
		Person p3 = new Person();
		p1.setName("DAN");
		p1.setCnp("1255343276456");
		p3.setCnp("1255343276456");
		Account a1 = new SavingAccount();
		Account a2 = new SpendingAccount();
		a1.setMoney(300);
		a2.setMoney(400);
		a1.setAccountNumber("223");
		a2.setAccountNumber("321");
		Set<Account> as = new HashSet<Account>();
		as.add(a2);
		as.add(a1);
		bank.addNewClient(p1,null);
		bank.addNewClientAccount(p1, a1);
		bank.addNewClientAccount(p1, a2);
		bank.addNewClient(p3,null);
		bank.addNewClientAccount(p3, a2);
		
		bank.writeData();
		//HashMap<Person, Set<Account>> map = new HashMap<Person,Set<Account>>();
		bank.readData();
		//map = bank.getHashMap();
		
		for(Map.Entry<Person, Set<Account>> entry : bank.getHashMap().entrySet()) {
			
			p2 = entry.getKey();
			as = entry.getValue();
			System.out.println(p2);
			System.out.println(as.toString());
		}
		
		System.out.println(p2);
		System.out.println(as.toString());
		
//		Person pTest = new Person();
//		pTest.setMail("sigur");
//		System.out.println(pTest.toString());
		
	}

}
