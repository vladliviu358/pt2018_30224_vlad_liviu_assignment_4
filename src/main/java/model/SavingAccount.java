package model;
import java.io.Serializable;

public class SavingAccount extends Account implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public SavingAccount() {
		super();
	}
	
	public void depositMoney(double money) {
		
		assert(money <=10000):"Maximul sumei este de 10000 RON";
		assert(money > 1):"Minimul sumei este de 1 ROM";
		double oldBalance = super.getMoney();
		double newBalance = oldBalance + money;
		super.setMoney(newBalance);
		assert(oldBalance < newBalance + 1):"Eroare la depunere";
	}
}
