package model;

import java.io.Serializable;

public abstract class Account implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private String accountNumber;
	private double money;
	private String expDate;

	public Account() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		assert checkNameFormat(name) : "Numele contului este invalid";
		this.name = name;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public boolean checkNameFormat(String name) {

		if (Character.isDigit(name.charAt(0)) == true)
			return false;
		return true;
	}

//	public int hashCode() {
//
//		int hash = 1;
//		int code;
//		code = Integer.parseInt(accountNumber);
//		hash = hash * 37 + code;
//		return hash;
//	}
	public String getAccountType(Account a) {
		String s="";
		if(a instanceof SavingAccount) {
			s = "Saving Account";
		}
		if (a instanceof SpendingAccount) {
			s="Spending Account";
		}
		
		return s;
	}

	public String toString() {

		return "Name: " + this.getName() + " AccountNumber: " + this.getAccountNumber() + " Balance: "
				+ this.getMoney();
	}
}
